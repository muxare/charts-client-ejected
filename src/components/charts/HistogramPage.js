import React from 'react';
import Histogram from '../common/Histogram';

const HistogramPage = () => (
    <div>
        <h1>Histogram Chart Page</h1>
        <Histogram />
    </div>
);

export default HistogramPage;
