import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => (
  <header className="page-header">
    <nav className="navbar navbar-default navbar-fixed-top">
        <ul className="nav navbar-nav">
            <li><Link to="/charts/barchart">Charts</Link></li>
            <li><Link to="/todo">Todo App</Link></li>
            <li><Link to="/users">Users</Link></li>
            <li><Link to="/finance">Finance</Link></li>
        </ul>
    </nav>
  </header>
);

export default Header;
