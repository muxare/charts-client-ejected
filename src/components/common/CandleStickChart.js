import React from 'react';
import './CandleStickChart.css'
import  {scaleLinear, scaleTime} from 'd3-scale';
//import  {XAxis, YAxis} from './axis';
import {timeParse} from 'd3-time-format';
import { max, min } from 'd3-array';

const margin = {top: 25, right: 25, bottom: 25, left: 25};
const widthSvg = 900;
const heightSvg = 500;
const width = widthSvg - margin.left - margin.right;
const height = heightSvg - margin.top - margin.bottom;

const dateFormat = timeParse("%Y-%m-%d");

function minimum(a, b) { return a < b ? a : b; }
function maximum(a, b) { return a > b ? a : b; }

const y = scaleLinear().range([height - margin.bottom, margin.top]);
const x = scaleTime().range([margin.left, width - margin.right]);

class CandleStickChart extends React.Component {
    render() {
        const {data} = this.props;
        y.domain([
            min(data.filter(x => x["Low"] > 0).map(function(x) {return x["Low"];})),
            max(data.map(function(x){return x["High"];}))
        ]);
        x.domain([min(data.map(function(d){return dateFormat(d.Date).getTime();})),
		  	   max(data.map(function(d){return dateFormat(d.Date).getTime();}))]);

        const dataLength = data.length;
        return (
            <svg className="chart" width={widthSvg} height={heightSvg}>
                {x.ticks().map((d, i) => <line key={i} className="x" x1={x(d)} x2={x(d)} y1={margin.top} y2={height - margin.bottom} stroke="#ccc"/>)}
                {y.ticks().map((d, i) => <line key={i} className="y" y1={y(d)} y2={y(d)} x1={margin.left} x2={width - margin.right} stroke="#ccc"/>)}
                {x.ticks().map((d, i) => {
                    const date = new Date(d);
                    return <text key={i} transform="" className="xrule" x={x(d)} y={height - margin.top} dy={20} textAnchor="middle">{`${(date.getMonth() + 1)}/${date.getDate()}`}</text>
                })}
                {y.ticks().map((d, i) => {
                    return <text key={i} className="yrule" x={width - margin.left} y={y(d)} dy={0} dx={20} textAnchor="middle">{d}</text>
                })}

                {data.map((d, i) => <rect
                        key={i}
                        x={x(dateFormat(d.Date).getTime()) - 0.25*(width - margin.left - margin.right)/dataLength}
                        y={y(maximum(d.Open, d.Close))}
                        height={y(minimum(d.Open, d.Close)) - y(maximum(d.Open, d.Close))}
                        width={0.5 * (width - margin.left - margin.right)/dataLength}
                        fill={d.Open > d.Close ? "red" : "green"}
					><title key={i}><div>{d.Date}</div><div>{d.Open}</div></title></rect>
                )}
                {data.map((d, i) => <line
                        key={i}
                        className="line"
                        x1={x(dateFormat(d.Date).getTime()) + 0.25 * (width - margin.left - margin.right)/dataLength - 0.25 * (width - margin.left - margin.right)/dataLength}
                        x2={x(dateFormat(d.Date).getTime()) + 0.25 * (width - margin.left - margin.right)/dataLength - 0.25 * (width - margin.left - margin.right)/dataLength}
                        y1={y(d.High)}
                        y2={y(d.Low)}
                        stroke={d.Open > d.Close ? "red" : "green"}
                    />
                )}
            </svg>
        );
    }
}
export default CandleStickChart;

export const CandleStickChartOrig2 = () => (
	<div id="chart" className="chart">
		<svg width="660" height="400">
			<g transform="translate(50,20)">
				<g clipPath="url(#plotAreaClip)">
					<clipPath id="plotAreaClip">
						<rect width="590" height="350"> </rect>
					</clipPath>
					<g className="series">
						<g className="candlestick-series">
							<g className="bar up-day">
								<path className="high-low-line" d="M-553.6923076923077,31.077581275068837L-553.6923076923077,68.75366124931861"> </path>
								<rect x="-558.6923076923077" y="34.33586318091113" width="10" height="29.300500455452493"> </rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M-535.5384615384615,17.374825889011674L-535.5384615384615,106.54146766004581"> </path>
								<rect x="-540.5384615384615" y="30.11346587821629" width="10" height="70.51132120877386"> </rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M-517.3846153846154,79.35354833405353L-517.3846153846154,128.3087590838667"> </path>
								<rect x="-522.3846153846154" y="100.11714290825827" width="10" height="1.0080735172720097"> </rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M-499.2307692307692,49.42638809394964L-499.2307692307692,100.15440612317755"> </path>
								<rect x="-504.2307692307692" y="73.55216597285448" width="10" height="26.60224015032307"> </rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M-444.7692307692308,35.41439951531311L-444.7692307692308,87.40112121396584"> </path>
								<rect x="-449.7692307692308" y="56.89990986610832" width="10" height="12.628699995085356"> </rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M-426.6153846153846,20.653110990732216L-426.6153846153846,106.04725928246768"> </path>
								<rect x="-431.6153846153846" y="68.11974858833219" width="10" height="24.942648685325764"> </rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M-408.46153846153845,89.2920575632362L-408.46153846153845,144.60310908883105">
								</path>
								<rect x="-413.46153846153845" y="89.2920575632362" width="10" height="42.37302556953574">
								</rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M-390.3076923076923,94.71325374935086L-390.3076923076923,138.8661811929964">
								</path>
								<rect x="-395.3076923076923" y="129.05079885389483" width="10" height="1.4938134029919752">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M-372.15384615384613,107.02630601649463L-372.15384615384613,143.52482927779835">
								</path>
								<rect x="-377.15384615384613" y="127.08401428055461" width="10" height="0.5389053408386815">
								</rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M-317.6923076923077,89.7211558152913L-317.6923076923077,126.03899067038185">
								</path>
								<rect x="-322.6923076923077" y="112.39341803922892" width="10" height="13.64557263115293">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M-299.53846153846155,96.15569674290921L-299.53846153846155,166.20686230712803">
								</path>
								<rect x="-304.53846153846155" y="110.38718496063223" width="10" height="48.905884741883625">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M-281.3846153846154,163.20368267267622L-281.3846153846154,191.25006904155063">
								</path>
								<rect x="-286.3846153846154" y="165.16662320389602" width="10" height="21.50577180603156">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M-263.2307692307692,181.05271065907712L-263.2307692307692,233.76242106834013">
								</path>
								<rect x="-268.2307692307692" y="181.05271065907712" width="10" height="36.161155754713945">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M-245.0769230769231,191.25591684703588L-245.0769230769231,255.71991676690146">
								</path>
								<rect x="-250.0769230769231" y="214.37206580021612" width="10" height="33.44474715995486">
								</rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M-190.6153846153846,189.98387910889468L-190.6153846153846,247.92673089546844">
								</path>
								<rect x="-195.6153846153846" y="197.19083573807396" width="10" height="50.73589515739448">
								</rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M-172.46153846153848,141.04621550802895L-172.46153846153848,205.9239927079823">
								</path>
								<rect x="-177.46153846153848" y="150.17578193331" width="10" height="39.288402736914776">
								</rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M-154.30769230769232,108.06402005957008L-154.30769230769232,163.5488091501557">
								</path>
								<rect x="-159.30769230769232" y="108.06402005957008" width="10" height="34.312432368371816">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M-136.15384615384616,104.14593356686623L-136.15384615384616,173.24043209251013">
								</path>
								<rect x="-141.15384615384616" y="112.70525650721365" width="10" height="59.87408718508078">
								</rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M-118,73.21732739119346L-118,171.0127955343013">
								</path>
								<rect x="-123" y="109.18405539641364" width="10" height="55.81375563821382">
								</rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M-63.53846153846154,50.49681027382269L-63.53846153846154,130.9842285776389">
								</path>
								<rect x="-68.53846153846155" y="58.74841762466889" width="10" height="54.849026656503014">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M-45.38461538461539,28.15532502152689L-45.38461538461539,100.61527559317511">
								</path>
								<rect x="-50.38461538461539" y="55.52761168092633" width="10" height="43.499378326143585">
								</rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M-27.230769230769234,74.43206211471869L-27.230769230769234,124.8223008043563">
								</path>
								<rect x="-32.23076923076923" y="82.47915419981757" width="10" height="24.377000896723587">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M-9.076923076923077,65.21134015677859L-9.076923076923077,129.07920492778285">
								</path>
								<rect x="-14.076923076923077" y="86.79348693285851" width="10" height="31.633832309233128">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M9.076923076923077,105.62085301554436L9.076923076923077,166.62687335500615">
								</path>
								<rect x="4.076923076923077" y="119.77073068674608" width="10" height="25.88831554442794">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M63.53846153846154,119.01087997003916L63.53846153846154,188.3662292897455">
								</path>
								<rect x="58.53846153846154" y="150.70170643274434" width="10" height="25.499089549439645">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M81.6923076923077,131.83922065597062L81.6923076923077,177.81257008916768">
								</path>
								<rect x="76.6923076923077" y="165.55950153185339" width="10" height="12.253068557314293">
								</rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M99.84615384615385,157.8866267579846L99.84615384615385,198.09981785883122">
								</path>
								<rect x="94.84615384615385" y="161.48544999346691" width="10" height="22.08395559162969">
								</rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M118,132.27530405234606L118,185.67237132880283">
								</path>
								<rect x="113" y="142.58737516396317" width="10" height="10.247480279400634">
								</rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M136.15384615384616,88.74800605475997L136.15384615384616,155.28337508758975">
								</path>
								<rect x="131.15384615384616" y="98.25824026610223" width="10" height="54.535910837022726">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M190.6153846153846,85.23051794636712L190.6153846153846,110.80537627839729">
								</path>
								<rect x="185.6153846153846" y="100.88489062328031" width="10" height="8.010125728898629">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M208.76923076923077,79.36741012332635L208.76923076923077,125.46268318643624">
								</path>
								<rect x="203.76923076923077" y="104.66450470787521" width="10" height="20.798178478561027">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M226.92307692307693,137.60536964095886L226.92307692307693,194.61993899700022">
								</path>
								<rect x="221.92307692307693" y="138.04215428579596" width="10" height="22.27284760092968">
								</rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M245.0769230769231,104.53660654939122L245.0769230769231,171.19603903621962">
								</path>
								<rect x="240.0769230769231" y="139.12566022359053" width="10" height="28.458220320420395">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M263.2307692307692,138.0664703395836L263.2307692307692,193.34836695335207">
								</path>
								<rect x="258.2307692307692" y="138.0664703395836" width="10" height="47.91934728026192">
								</rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M317.6923076923077,167.74994952505864L317.6923076923077,199.56534253718675">
								</path>
								<rect x="312.6923076923077" y="188.07258092574" width="10" height="4.654894476498413">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M335.8461538461538,190.8596673578122L335.8461538461538,237.8174082076879">
								</path>
								<rect x="330.8461538461538" y="190.8596673578122" width="10" height="24.98612986359322">
								</rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M354,188.83095923619118L354,221.16129246882497">
								</path>
								<rect x="349" y="203.3070852121076" width="10" height="9.52006605186503">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M372.15384615384613,189.7061934446251L372.15384615384613,237.34838978600226">
								</path>
								<rect x="367.15384615384613" y="199.32469907752153" width="10" height="28.365999247557227">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M390.3076923076923,218.44207870232384L390.3076923076923,271.33404029038996">
								</path>
								<rect x="385.3076923076923" y="224.15219503101116" width="10" height="27.898303292434832">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M444.7692307692308,224.95317486125634L444.7692307692308,250.2042741182326">
								</path>
								<rect x="439.7692307692308" y="245.68368992246482" width="10" height="0.48599736366259094">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M462.9230769230769,237.06668728846512L462.9230769230769,305.2317277524403">
								</path>
								<rect x="457.9230769230769" y="249.00674173115544" width="10" height="52.20560778062662">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M481.0769230769231,300.7970998735822L481.0769230769231,326.90583356482006">
								</path>
								<rect x="476.0769230769231" y="302.8122778168943" width="10" height="21.396116013958306">
								</rect>
							</g>
							<g className="bar down-day">
								<path className="high-low-line" d="M499.2307692307692,295.56185299202735L499.2307692307692,328.68281572470266">
								</path>
								<rect x="494.2307692307692" y="322.91115468943667" width="10" height="0.36925812779196576">
								</rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M517.3846153846154,275.0503658209431L517.3846153846154,322.14094956856496">
								</path>
								<rect x="512.3846153846154" y="318.3127806035596" width="10" height="1.5583656594893682">
								</rect>
							</g>
							<g className="bar up-day">
								<path className="high-low-line" d="M571.8461538461538,264.1871825857578L571.8461538461538,316.28616730407">
								</path>
								<rect x="566.8461538461538" y="297.56286286057485" width="10" height="18.723304443495124">
								</rect>
							</g>
						</g>
					</g>
				</g>
				<g className="x axis" transform="translate(0,350)">
					<g className="tick" transform="translate(45.38461538461539,0)" style={{opacity: "1"}}>
						<line y2="6" x2="0">
						</line>
						<text y="9" x="0" dy=".71em" style={{textAnchor: "middle"}}>
							Aug 03</text>
					</g>
					<g className="tick" transform="translate(172.46153846153848,0)" style={{opacity: "1"}}>
						<line y2="6" x2="0">
						</line>
						<text y="9" x="0" dy=".71em" style={{textAnchor: "middle"}}>
							Aug 10</text>
					</g>
					<g className="tick" transform="translate(299.53846153846155,0)" style={{opacity: "1"}}>
						<line y2="6" x2="0">
						</line>
						<text y="9" x="0" dy=".71em" style={{textAnchor: "middle"}}>
							Aug 17</text>
					</g>
					<g className="tick" transform="translate(426.6153846153846,0)" style={{opacity: "1"}}>
						<line y2="6" x2="0">
						</line>
						<text y="9" x="0" dy=".71em" style={{textAnchor: "middle"}}>
							Aug 24</text>
					</g>
					<g className="tick" transform="translate(553.6923076923077,0)" style={{opacity: "1"}}>
						<line y2="6" x2="0">
						</line>
						<text y="9" x="0" dy=".71em" style={{textAnchor: "middle"}}>
							Aug 31</text>
					</g>
					<path className="domain" d="M0,6V0H590V6"> </path>
				</g>
				<g className="y axis">
					<g className="tick" transform="translate(0,350)" style={{opacity: "1"}}>
						<line x2="-6" y2="0">
						</line>
						<text x="-9" y="0" dy=".32em" style={{textAnchor: "end"}}>
							95.5</text>
					</g>
					<g className="tick" transform="translate(0,318.1818181818182)" style={{opacity: "1"}}>
						<line x2="-6" y2="0">
						</line>
						<text x="-9" y="0" dy=".32em" style={{textAnchor: "end"}}>
							96.0</text>
					</g>
					<g className="tick" transform="translate(0,286.3636363636364)" style={{opacity: "1"}}>
						<line x2="-6" y2="0">
						</line>
						<text x="-9" y="0" dy=".32em" style={{textAnchor: "end"}}>
							96.5</text>
					</g>
					<g className="tick" transform="translate(0,254.54545454545456)" style={{opacity: "1"}}>
						<line x2="-6" y2="0">
						</line>
						<text x="-9" y="0" dy=".32em" style={{textAnchor: "end"}}>
							97.0</text>
					</g>
					<g className="tick" transform="translate(0,222.72727272727272)" style={{opacity: "1"}}>
						<line x2="-6" y2="0">
						</line>
						<text x="-9" y="0" dy=".32em" style={{textAnchor: "end"}}>
							97.5</text>
					</g>
					<g className="tick" transform="translate(0,190.9090909090909)" style={{opacity: "1"}}>
						<line x2="-6" y2="0">
						</line>
						<text x="-9" y="0" dy=".32em" style={{textAnchor: "end"}}>
							98.0</text>
					</g>
					<g className="tick" transform="translate(0,159.0909090909091)" style={{opacity: "1"}}>
						<line x2="-6" y2="0">
						</line>
						<text x="-9" y="0" dy=".32em" style={{textAnchor: "end"}}>
							98.5</text>
					</g>
					<g className="tick" transform="translate(0,127.27272727272728)" style={{opacity: "1"}}>
						<line x2="-6" y2="0"> </line>
						<text x="-9" y="0" dy=".32em" style={{textAnchor: "end"}}> 99.0</text>
					</g>
					<g className="tick" transform="translate(0,95.45454545454544)" style={{opacity: "1"}}>
						<line x2="-6" y2="0">
						</line>
						<text x="-9" y="0" dy=".32em" style={{textAnchor: "end"}}>
							99.5</text>
					</g>
					<g className="tick" transform="translate(0,63.636363636363626)" style={{opacity: "1"}}>
						<line x2="-6" y2="0">
						</line>
						<text x="-9" y="0" dy=".32em" style={{textAnchor: "end"}}>
							100.0</text>
					</g>
					<g className="tick" transform="translate(0,31.818181818181813)" style={{opacity: "1"}}>
						<line x2="-6" y2="0"> </line>
						<text x="-9" y="0" dy=".32em" style={{textAnchor: "end"}}> 100.5</text>
					</g>
					<g className="tick" transform="translate(0,0)" style={{opacity: "1"}}>
						<line x2="-6" y2="0"> </line>
						<text x="-9" y="0" dy=".32em" style={{textAnchor: "end"}}> 101.0</text>
					</g>
					<path className="domain" d="M-6,0H0V350H-6"> </path>
				</g>
			</g>
		</svg>
	</div>
);

// http://bl.ocks.org/abeppu/1074045
export const CandleStickChartOrig = () => (
    <svg className="chart" width="900" height="500">
        <line className="x" x1="111.96862553848655" x2="111.96862553848655" y1="50" y2="450" stroke="#ccc"> </line>
        <line className="x" x1="193.24961391530522" x2="193.24961391530522" y1="50" y2="450" stroke="#ccc"> </line>
        <line className="x" x1="274.5306022921239" x2="274.5306022921239" y1="50" y2="450" stroke="#ccc"> </line>
        <line className="x" x1="355.81159066894253" x2="355.81159066894253" y1="50" y2="450" stroke="#ccc"> </line>
        <line className="x" x1="437.0925790457612" x2="437.0925790457612" y1="50" y2="450" stroke="#ccc"> </line>
        <line className="x" x1="518.3735674225798" x2="518.3735674225798" y1="50" y2="450" stroke="#ccc"> </line>
        <line className="x" x1="599.6545557993985" x2="599.6545557993985" y1="50" y2="450" stroke="#ccc"> </line>
        <line className="x" x1="680.9355441762171" x2="680.9355441762171" y1="50" y2="450" stroke="#ccc"> </line>
        <line className="x" x1="762.2165325530359" x2="762.2165325530359" y1="50" y2="450" stroke="#ccc"> </line>
        <line className="x" x1="843.4975209298545" x2="843.4975209298545" y1="50" y2="450" stroke="#ccc"> </line>
        <line className="y" x1="50" x2="850" y1="429.8901659647587" y2="429.8901659647587" stroke="#ccc"> </line>
        <line className="y" x1="50" x2="850" y1="398.95195975669515" y2="398.95195975669515" stroke="#ccc"> </line>
        <line className="y" x1="50" x2="850" y1="368.0137535486316" y2="368.0137535486316" stroke="#ccc"> </line>
        <line className="y" x1="50" x2="850" y1="337.07554734056805" y2="337.07554734056805" stroke="#ccc"> </line>
        <line className="y" x1="50" x2="850" y1="306.1373411325045" y2="306.1373411325045" stroke="#ccc"> </line>
        <line className="y" x1="50" x2="850" y1="275.19913492444095" y2="275.19913492444095" stroke="#ccc"> </line>
        <line className="y" x1="50" x2="850" y1="244.2609287163774" y2="244.2609287163774" stroke="#ccc"> </line>
        <line className="y" x1="50" x2="850" y1="213.32272250831386" y2="213.32272250831386" stroke="#ccc"> </line>
        <line className="y" x1="50" x2="850" y1="182.3845163002503" y2="182.3845163002503" stroke="#ccc"> </line>
        <line className="y" x1="50" x2="850" y1="151.44631009218676" y2="151.44631009218676" stroke="#ccc"> </line>
        <line className="y" x1="50" x2="850" y1="120.50810388412322" y2="120.50810388412322" stroke="#ccc"> </line>
        <line className="y" x1="50" x2="850" y1="89.56989767605967" y2="89.56989767605967" stroke="#ccc"> </line>
        <line className="y" x1="50" x2="850" y1="58.63169146799612" y2="58.63169146799612" stroke="#ccc"> </line>
        <text className="xrule" x="111.96862553848655" y="450" dy="20" textAnchor="middle">3/22</text>
        <text className="xrule" x="193.24961391530522" y="450" dy="20" textAnchor="middle">1/24</text>
        <text className="xrule" x="274.5306022921239" y="450" dy="20" textAnchor="middle">11/28</text>
        <text className="xrule" x="355.81159066894253" y="450" dy="20" textAnchor="middle">10/2</text>
        <text className="xrule" x="437.0925790457612" y="450" dy="20" textAnchor="middle">8/6</text>
        <text className="xrule" x="518.3735674225798" y="450" dy="20" textAnchor="middle">6/10</text>
        <text className="xrule" x="599.6545557993985" y="450" dy="20" textAnchor="middle">4/14</text>
        <text className="xrule" x="680.9355441762171" y="450" dy="20" textAnchor="middle">2/16</text>
        <text className="xrule" x="762.2165325530359" y="450" dy="20" textAnchor="middle">12/22</text>
        <text className="xrule" x="843.4975209298545" y="450" dy="20" textAnchor="middle">10/26</text>
        <text className="yrule" x="850" y="429.8901659647587" dy="0" dx="20" textAnchor="middle">840</text>
        <text className="yrule" x="850" y="398.95195975669515" dy="0" dx="20" textAnchor="middle">850</text>
        <text className="yrule" x="850" y="368.0137535486316" dy="0" dx="20" textAnchor="middle">860</text>
        <text className="yrule" x="850" y="337.07554734056805" dy="0" dx="20" textAnchor="middle">870</text>
        <text className="yrule" x="850" y="306.1373411325045" dy="0" dx="20" textAnchor="middle">880</text>
        <text className="yrule" x="850" y="275.19913492444095" dy="0" dx="20" textAnchor="middle">890</text>
        <text className="yrule" x="850" y="244.2609287163774" dy="0" dx="20" textAnchor="middle">900</text>
        <text className="yrule" x="850" y="213.32272250831386" dy="0" dx="20" textAnchor="middle">910</text>
        <text className="yrule" x="850" y="182.3845163002503" dy="0" dx="20" textAnchor="middle">920</text>
        <text className="yrule" x="850" y="151.44631009218676" dy="0" dx="20" textAnchor="middle">930</text>
        <text className="yrule" x="850" y="120.50810388412322" dy="0" dx="20" textAnchor="middle">940</text>
        <text className="yrule" x="850" y="89.56989767605967" dy="0" dx="20" textAnchor="middle">950</text>
        <text className="yrule" x="850" y="58.63169146799612" dy="0" dx="20" textAnchor="middle">960</text>
        <rect x="50" y="382.554747592269" height="5.816398236218959" width="9.75609756097561" fill="red"> </rect>
        <rect x="64.04535479151426" y="388.15455672828705" height="3.6506835819868115" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="106.18141916605705" y="377.3881197504729" height="16.892139930598546" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="120.22677395757133" y="371.60250193561035" height="48.38740091672054" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="134.2721287490856" y="404.95397794870087" height="23.605866805855555" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="148.31748354059985" y="404.52079974829905" height="2.5369545658056722" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="162.3628383321141" y="393.75436277048493" height="18.77951591885926" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="203.9136795903438" y="408.7902876741149" height="27.070930432055604" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="217.9590343818581" y="380.389036031857" height="13.148737638427008" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="232.00438917337237" y="323.71022060194014" height="47.24269966230497" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="246.0497439648866" y="317.46064107149914" height="4.300457070229982" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="260.0950987564009" y="285.90382233648455" height="29.514980658439015" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="302.2311631309437" y="270.5274348488173" height="10.859341317236385" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="316.27651792245797" y="223.13008128131946" height="47.42832271191196" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="330.3218727139722" y="210.78576794250822" height="4.764415691988205" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="344.3672275054864" y="201.56624127509718" height="48.01596918826911" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="358.41258229700077" y="245.34369168196463" height="14.757583143838247" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="400.54864667154357" y="222.48049960995425" height="22.92512726701844" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="414.5940014630578" y="222.48049960995425" height="14.479058848629307" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="428.6393562545721" y="234.70093946492915" height="21.223754868300745" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="442.6847110460863" y="270.71305789842427" height="20.9761935298651" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="498.8661302121434" y="238.10425661917884" height="44.829429857278" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="512.9114850036576" y="232.56619704893134" height="8.631880191053881" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="526.9568397951718" y="220.0052915160989" height="26.73065657107611" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="541.0021945866862" y="237.8876644251577" height="7.301373351614103" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="555.0475493782004" y="236.000480253775" height="12.808275054389668" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="597.1836137527432" y="217.4065873845227" height="3.9292140648362874" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="611.2289685442576" y="220.68603105493602" height="1.794468555018227" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="625.2743233357718" y="212.39461344791945" height="3.1247897652204983" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="639.319678127286" y="187.39649023685354" height="12.344313338811247" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="653.3650329188002" y="93.18961520745256" height="73.75676713318012" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="695.5010972933432" y="95.04602205129947" height="63.20673053250874" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="709.5464520848574" y="99.03698258808572" height="0.8971399159802331" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="723.5918068763716" y="101.94518015928509" height="15.376198764609512" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="737.6371616678858" y="105.81245593529303" height="22.33729516142381" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="751.6825164594002" y="118.89925528489135" height="19.707624979254035" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="793.818580833943" y="92.54003353608738" height="25.02890363242227" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="807.8639356254572" y="80.84530186864129" height="0.06193519500806133" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="821.9092904169715" y="78.74152550323743" height="14.076846698821441" width="9.75609756097561"
              fill="red"> </rect>
        <rect x="835.9546452084858" y="96.93320622268186" height="7.7655206964300305" width="9.75609756097561"
              fill="green"> </rect>
        <rect x="850" y="54.455107881602544" height="21.192597000828528" width="9.75609756097561" fill="green"> </rect>
        <line className="stem" x1="54.8780487804878" x2="54.8780487804878" y1="381.9359463422602" y2="397.3740803018778"
              stroke="red"> </line>
        <line className="stem" x1="68.92340357200206" x2="68.92340357200206" y1="387.10257418405627" y2="396.97186815206993"
              stroke="red"> </line>
        <line className="stem" x1="111.05946794654486" x2="111.05946794654486" y1="374.8201960402531"
              y2="395.82716999147465" stroke="green"> </line>
        <line className="stem" x1="125.10482273805913" x2="125.10482273805913" y1="359.3510929362213"
              y2="425.83726713914376" stroke="red"> </line>
        <line className="stem" x1="139.1501775295734" x2="139.1501775295734" y1="400.9010822169062" y2="432.8293326803722"
              stroke="green"> </line>
        <line className="stem" x1="153.19553232108765" x2="153.19553232108765" y1="396.19841299686834"
              y2="415.03986411073566" stroke="red"> </line>
        <line className="stem" x1="167.2408871126019" x2="167.2408871126019" y1="393.3831197650912" y2="418.9688894525142"
              stroke="red"> </line>
        <line className="stem" x1="208.7917283708316" x2="208.7917283708316" y1="398.02385069630077" y2="450"
              stroke="green"> </line>
        <line className="stem" x1="222.8370831623459" x2="222.8370831623459" y1="372.7781692406198" y2="398.6426519463095"
              stroke="green"> </line>
        <line className="stem" x1="236.88243795386018" x2="236.88243795386018" y1="317.15133635493373"
              y2="371.04563588060927" stroke="green"> </line>
        <line className="stem" x1="250.9277927453744" x2="250.9277927453744" y1="315.2331799453166" y2="331.9398886431861"
              stroke="green"> </line>
        <line className="stem" x1="264.9731475368887" x2="264.9731475368887" y1="274.11637195885373" y2="316.5015659605108"
              stroke="green"> </line>
        <line className="stem" x1="307.1092119114315" x2="307.1092119114315" y1="264.40173189603286" y2="289.3688859626846"
              stroke="green"> </line>
        <line className="stem" x1="321.1545667029458" x2="321.1545667029458" y1="217.83976867874472" y2="274.33277542981716"
              stroke="green"> </line>
        <line className="stem" x1="335.19992149446" x2="335.19992149446" y1="170.87559331164874" y2="226.87367229654876"
              stroke="red"> </line>
        <line className="stem" x1="349.2452762859742" x2="349.2452762859742" y1="191.07814605707478" y2="261.3079112752265"
              stroke="red"> </line>
        <line className="stem" x1="363.29063107748857" x2="363.29063107748857" y1="243.98240132734816"
              y2="277.33387734043873" stroke="red"> </line>
        <line className="stem" x1="405.42669545203137" x2="405.42669545203137" y1="217.93248429510922"
              y2="247.35474933718376" stroke="green"> </line>
        <line className="stem" x1="419.4720502435456" x2="419.4720502435456" y1="209.48641587672006" y2="251.9954802683933"
              stroke="red"> </line>
        <line className="stem" x1="433.5174050350599" x2="433.5174050350599" y1="231.60711884412274" y2="258.9565766652076"
              stroke="red"> </line>
        <line className="stem" x1="447.5627598265741" x2="447.5627598265741" y1="259.82293615983144" y2="292.24611748329005"
              stroke="red"> </line>
        <line className="stem" x1="503.7441789926312" x2="503.7441789926312" y1="236.89762016975524" y2="282.93368647645684"
              stroke="green"> </line>
        <line className="stem" x1="517.7895337841454" x2="517.7895337841454" y1="214.52935895773751" y2="241.84765891135038"
              stroke="green"> </line>
        <line className="stem" x1="531.8348885756596" x2="531.8348885756596" y1="211.77581219791065" y2="252.39769241820116"
              stroke="red"> </line>
        <line className="stem" x1="545.880243367174" x2="545.880243367174" y1="227.80178135694317" y2="255.7390712836226"
              stroke="green"> </line>
        <line className="stem" x1="559.9255981586882" x2="559.9255981586882" y1="232.9684091987392" y2="254.25390744516938"
              stroke="red"> </line>
        <line className="stem" x1="602.061662533231" x2="602.061662533231" y1="213.353691652728" y2="232.4425122881527"
              stroke="red"> </line>
        <line className="stem" x1="616.1070173247454" x2="616.1070173247454" y1="214.93157110754572" y2="234.97946685395834"
              stroke="green"> </line>
        <line className="stem" x1="630.1523721162596" x2="630.1523721162596" y1="195.53325393867732" y2="220.87165101072276"
              stroke="red"> </line>
        <line className="stem" x1="644.1977269077738" x2="644.1977269077738" y1="176.63005635285964" y2="206.79480740572157"
              stroke="green"> </line>
        <line className="stem" x1="658.243081699288" x2="658.243081699288" y1="90.8382805974336" y2="168.9882204172082"
              stroke="red"> </line>
        <line className="stem" x1="700.379146073831" x2="700.379146073831" y1="75.9570126928167" y2="158.25275258380822"
              stroke="green"> </line>
        <line className="stem" x1="714.4245008653452" x2="714.4245008653452" y1="89.26058986567404" y2="116.14590034194288"
              stroke="green"> </line>
        <line className="stem" x1="728.4698556568594" x2="728.4698556568594" y1="101.94518015928509" y2="133.19269417773427"
              stroke="red"> </line>
        <line className="stem" x1="742.5152104483736" x2="742.5152104483736" y1="105.03900078009144" y2="138.39047679318196"
              stroke="red"> </line>
        <line className="stem" x1="756.560565239888" x2="756.560565239888" y1="118.0640536577398" y2="150.51820103179233"
              stroke="red"> </line>
        <line className="stem" x1="798.6966296144308" x2="798.6966296144308" y1="92.50906439167318" y2="122.95215411050663"
              stroke="green"> </line>
        <line className="stem" x1="812.741984405945" x2="812.741984405945" y1="65.15960657058838" y2="88.95109642605087"
              stroke="green"> </line>
        <line className="stem" x1="826.7873391974593" x2="826.7873391974593" y1="77.96807034803584" y2="105.03900078009144"
              stroke="red"> </line>
        <line className="stem" x1="840.8326939889736" x2="840.8326939889736" y1="88.67275776007943" y2="118.09483407909619"
              stroke="green"> </line>
        <line className="stem" x1="854.8780487804878" x2="854.8780487804878" y1="50" y2="84.83626240542787"
              stroke="green"> </line>
    </svg>
);
