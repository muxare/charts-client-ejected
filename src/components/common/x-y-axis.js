import React  from 'react';
import {XAxis, YAxis} from './axis';

export default ({
    xScale, yScale, height, margin
}) => {
    const xSettings = {
        translate: `translate(${margin.left}, ${height + margin.top})`,
        scale: xScale,
        orient: 'bottom'
    };
    const ySettings = {
        translate: `translate(${margin.left}, ${margin.top})`,
        scale: yScale,
        orient: 'left'
    };
    return <g className="xy-axis">
        <XAxis {...xSettings}/>
        <YAxis {...ySettings}/>
    </g>
}