import React from 'react';
import { axisBottom, axisLeft } from 'd3-axis';
import { select } from 'd3-selection';

export class XAxis extends React.Component {
    componentDidMount() {
        this.renderAxis();
    }

    componentDidUpdate() {
        this.renderAxis();
    }

    renderAxis() {
        const node  = this.refs.axis;
        const axis = axisBottom().ticks(5).scale(this.props.scale);
        select(node).call(axis);
    }

    render() {
        return <g className="axis" ref="axis" transform={this.props.translate}/>
    }
}

export class YAxis extends React.Component {
    componentDidMount() {
        this.renderAxis();
    }

    componentDidUpdate() {
        this.renderAxis();
    }

    renderAxis() {
        const node  = this.refs.axis;
        const axis = axisLeft().ticks(5).scale(this.props.scale);
        select(node).call(axis);
    }

    render() {
        return <g className="axis" ref="axis" transform={this.props.translate}/>
    }
}