import React from 'react';
import './CalendarView.css';
//import { format } from 'd3-format';
import { timeFormat } from 'd3-time-format';
//import { scaleQuantize } from 'd3-scale';
import { range } from 'd3-array';
import { timeDays, timeWeek, timeYear } from 'd3-time';

//const margin = {top: 20, right: 20, bottom: 30, left: 50};
const svgProps = {
    width: 960,
    height: 136
};
const cellSize = 17;

//const formatPercent = format(".1%");
// const color = scaleQuantize()
//     .domain([-0.05, 0.05])
//     .range(["#a50026", "#d73027", "#f46d43", "#fdae61", "#fee08b", "#ffffbf", "#d9ef8b", "#a6d96a", "#66bd63", "#1a9850", "#006837"]);

const pathMonth = (t0) => {
   let t1 = new Date(t0.getFullYear(), t0.getMonth() + 1, 0),
      d0 = t0.getDay(), w0 = timeWeek.count(timeYear(t0), t0),
      d1 = t1.getDay(), w1 = timeWeek.count(timeYear(t1), t1);
  return "M" + (w0 + 1) * cellSize + "," + d0 * cellSize
      + "H" + w0 * cellSize + "V" + 7 * cellSize
      + "H" + w1 * cellSize + "V" + (d1 + 1) * cellSize
      + "H" + (w1 + 1) * cellSize + "V" + 0
      + "H" + (w0 + 1) * cellSize + "Z";
};

const Calendar = ({d, width, height, cellSize}) => (
    <svg width={width} height={height}>
        <g transform={`translate(${((width - cellSize * 53) / 2)}, ${(height - cellSize * 7 - 1)})`}>
            <text fontFamily="sans-serif" fontSize="10" textAnchor="middle" transform={`translate(${-6}, ${cellSize * 3.5})rotate(-90)`}>{d}</text>
            <g fill="none" stroke="#ccc">
                {timeDays(new Date(d, 0, 1), new Date(d + 1, 0, 1)).map((d, i) =>
                    <rect
                        key={i}
                        width={ cellSize }
                        height={ cellSize }
                        x={ timeWeek.count(timeYear(d), d) * cellSize }
                        y={ d.getDay() * cellSize }
                    >
                        {timeFormat("%Y-%m-%d")(d)}
                    </rect>
                )}
            </g>
            <g fill="none" stroke="#000">
                {timeDays(new Date(d, 0, 1), new Date(d + 1, 0, 1)).map((d, i) =>
                    <path key={i} d={pathMonth(d)} />
                )}
            </g>
        </g>
    </svg>
);

class CalendarView extends React.Component {
    render() {
        //const data = this.props.data;
        return (
            <div>
                {range(1990, 2011).map((d, i) =>
                    <Calendar
                        key={i}
                        d={d}
                        width={svgProps.width}
                        height={svgProps.height}
                        cellSize={cellSize}
                    />
                )}
            </div>
        );
    }
}
export default CalendarView;

export const CalendarViewOrig = () => (
    <svg width="960" height="136">
        <g transform="translate(29.5,16)">
            <text transform="translate(-6,59.5)rotate(-90)" fontFamily="sans-serif" fontSize="10"
                  textAnchor="middle">2007
            </text>
            <g fill="none" stroke="#ccc">
                <rect width="17" height="17" x="0" y="17"/>
                <rect width="17" height="17" x="0" y="34"/>
                <rect width="17" height="17" x="0" y="51" fill="#ffffbf"><title>2007-01-03: 0.1%</title></rect>
                <rect width="17" height="17" x="0" y="68" fill="#ffffbf"><title>2007-01-04: 0.1%</title></rect>
                <rect width="17" height="17" x="0" y="85" fill="#fee08b"><title>2007-01-05: -0.7%</title></rect>
                <rect width="17" height="17" x="0" y="102"/>
                <rect width="17" height="17" x="17" y="0"/>
                <rect width="17" height="17" x="17" y="17" fill="#ffffbf"><title>2007-01-08: 0.2%</title></rect>
                <rect width="17" height="17" x="17" y="34" fill="#ffffbf"><title>2007-01-09: -0.1%</title></rect>
                <rect width="17" height="17" x="17" y="51" fill="#ffffbf"><title>2007-01-10: 0.2%</title></rect>
                <rect width="17" height="17" x="17" y="68" fill="#d9ef8b"><title>2007-01-11: 0.6%</title></rect>
                <rect width="17" height="17" x="17" y="85" fill="#ffffbf"><title>2007-01-12: 0.3%</title></rect>
                <rect width="17" height="17" x="17" y="102"/>
                <rect width="17" height="17" x="34" y="0"/>
                <rect width="17" height="17" x="34" y="17"/>
                <rect width="17" height="17" x="34" y="34" fill="#ffffbf"><title>2007-01-16: 0.2%</title></rect>
                <rect width="17" height="17" x="34" y="51" fill="#ffffbf"><title>2007-01-17: 0.1%</title></rect>
                <rect width="17" height="17" x="34" y="68" fill="#ffffbf"><title>2007-01-18: -0.1%</title></rect>
                <rect width="17" height="17" x="34" y="85" fill="#ffffbf"><title>2007-01-19: 0.0%</title></rect>
                <rect width="17" height="17" x="34" y="102"/>
                <rect width="17" height="17" x="51" y="0"/>
                <rect width="17" height="17" x="51" y="17" fill="#fee08b"><title>2007-01-22: -0.7%</title></rect>
                <rect width="17" height="17" x="51" y="34" fill="#d9ef8b"><title>2007-01-23: 0.5%</title></rect>
                <rect width="17" height="17" x="51" y="51" fill="#d9ef8b"><title>2007-01-24: 0.7%</title></rect>
                <rect width="17" height="17" x="51" y="68" fill="#fee08b"><title>2007-01-25: -0.9%</title></rect>
                <rect width="17" height="17" x="51" y="85" fill="#ffffbf"><title>2007-01-26: -0.1%</title></rect>
                <rect width="17" height="17" x="51" y="102"/>
                <rect width="17" height="17" x="68" y="0"/>
                <rect width="17" height="17" x="68" y="17" fill="#ffffbf"><title>2007-01-29: 0.0%</title></rect>
                <rect width="17" height="17" x="68" y="34" fill="#ffffbf"><title>2007-01-30: 0.3%</title></rect>
                <rect width="17" height="17" x="68" y="51" fill="#d9ef8b"><title>2007-01-31: 0.8%</title></rect>
                <rect width="17" height="17" x="68" y="68" fill="#ffffbf"><title>2007-02-01: 0.4%</title></rect>
                <rect width="17" height="17" x="68" y="85" fill="#ffffbf"><title>2007-02-02: -0.2%</title></rect>
                <rect width="17" height="17" x="68" y="102"/>
                <rect width="17" height="17" x="85" y="0"/>
                <rect width="17" height="17" x="85" y="17" fill="#ffffbf"><title>2007-02-05: 0.2%</title></rect>
                <rect width="17" height="17" x="85" y="34" fill="#ffffbf"><title>2007-02-06: 0.0%</title></rect>
                <rect width="17" height="17" x="85" y="51" fill="#ffffbf"><title>2007-02-07: 0.1%</title></rect>
                <rect width="17" height="17" x="85" y="68" fill="#ffffbf"><title>2007-02-08: 0.0%</title></rect>
                <rect width="17" height="17" x="85" y="85" fill="#ffffbf"><title>2007-02-09: -0.5%</title></rect>
                <rect width="17" height="17" x="85" y="102"/>
                <rect width="17" height="17" x="102" y="0"/>
                <rect width="17" height="17" x="102" y="17" fill="#ffffbf"><title>2007-02-12: -0.3%</title></rect>
                <rect width="17" height="17" x="102" y="34" fill="#d9ef8b"><title>2007-02-13: 0.8%</title></rect>
                <rect width="17" height="17" x="102" y="51" fill="#d9ef8b"><title>2007-02-14: 0.7%</title></rect>
                <rect width="17" height="17" x="102" y="68" fill="#ffffbf"><title>2007-02-15: 0.2%</title></rect>
                <rect width="17" height="17" x="102" y="85" fill="#ffffbf"><title>2007-02-16: 0.0%</title></rect>
                <rect width="17" height="17" x="102" y="102"/>
                <rect width="17" height="17" x="119" y="0"/>
                <rect width="17" height="17" x="119" y="17"/>
                <rect width="17" height="17" x="119" y="34" fill="#ffffbf"><title>2007-02-20: 0.2%</title></rect>
                <rect width="17" height="17" x="119" y="51" fill="#ffffbf"><title>2007-02-21: -0.3%</title></rect>
                <rect width="17" height="17" x="119" y="68" fill="#ffffbf"><title>2007-02-22: -0.4%</title></rect>
                <rect width="17" height="17" x="119" y="85" fill="#ffffbf"><title>2007-02-23: -0.3%</title></rect>
                <rect width="17" height="17" x="119" y="102"/>
                <rect width="17" height="17" x="136" y="0"/>
                <rect width="17" height="17" x="136" y="17" fill="#ffffbf"><title>2007-02-26: -0.1%</title></rect>
                <rect width="17" height="17" x="136" y="34" fill="#d73027"><title>2007-02-27: -3.3%</title></rect>
                <rect width="17" height="17" x="136" y="51" fill="#ffffbf"><title>2007-02-28: 0.4%</title></rect>
                <rect width="17" height="17" x="136" y="68" fill="#ffffbf"><title>2007-03-01: -0.3%</title></rect>
                <rect width="17" height="17" x="136" y="85" fill="#fee08b"><title>2007-03-02: -1.0%</title></rect>
                <rect width="17" height="17" x="136" y="102"/>
                <rect width="17" height="17" x="153" y="0"/>
                <rect width="17" height="17" x="153" y="17" fill="#fee08b"><title>2007-03-05: -0.5%</title></rect>
                <rect width="17" height="17" x="153" y="34" fill="#d9ef8b"><title>2007-03-06: 1.3%</title></rect>
                <rect width="17" height="17" x="153" y="51" fill="#ffffbf"><title>2007-03-07: -0.1%</title></rect>
                <rect width="17" height="17" x="153" y="68" fill="#d9ef8b"><title>2007-03-08: 0.6%</title></rect>
                <rect width="17" height="17" x="153" y="85" fill="#ffffbf"><title>2007-03-09: 0.1%</title></rect>
                <rect width="17" height="17" x="153" y="102"/>
                <rect width="17" height="17" x="170" y="0"/>
                <rect width="17" height="17" x="170" y="17" fill="#ffffbf"><title>2007-03-12: 0.3%</title></rect>
                <rect width="17" height="17" x="170" y="34" fill="#fdae61"><title>2007-03-13: -1.9%</title></rect>
                <rect width="17" height="17" x="170" y="51" fill="#d9ef8b"><title>2007-03-14: 0.5%</title></rect>
                <rect width="17" height="17" x="170" y="68" fill="#ffffbf"><title>2007-03-15: 0.2%</title></rect>
                <rect width="17" height="17" x="170" y="85" fill="#ffffbf"><title>2007-03-16: -0.4%</title></rect>
                <rect width="17" height="17" x="170" y="102"/>
                <rect width="17" height="17" x="187" y="0"/>
                <rect width="17" height="17" x="187" y="17" fill="#d9ef8b"><title>2007-03-19: 1.0%</title></rect>
                <rect width="17" height="17" x="187" y="34" fill="#d9ef8b"><title>2007-03-20: 0.5%</title></rect>
                <rect width="17" height="17" x="187" y="51" fill="#d9ef8b"><title>2007-03-21: 1.3%</title></rect>
                <rect width="17" height="17" x="187" y="68" fill="#ffffbf"><title>2007-03-22: 0.1%</title></rect>
                <rect width="17" height="17" x="187" y="85" fill="#ffffbf"><title>2007-03-23: 0.2%</title></rect>
                <rect width="17" height="17" x="187" y="102"/>
                <rect width="17" height="17" x="204" y="0"/>
                <rect width="17" height="17" x="204" y="17" fill="#ffffbf"><title>2007-03-26: -0.1%</title></rect>
                <rect width="17" height="17" x="204" y="34" fill="#fee08b"><title>2007-03-27: -0.6%</title></rect>
                <rect width="17" height="17" x="204" y="51" fill="#fee08b"><title>2007-03-28: -0.8%</title></rect>
                <rect width="17" height="17" x="204" y="68" fill="#ffffbf"><title>2007-03-29: 0.4%</title></rect>
                <rect width="17" height="17" x="204" y="85" fill="#ffffbf"><title>2007-03-30: 0.0%</title></rect>
                <rect width="17" height="17" x="204" y="102"/>
                <rect width="17" height="17" x="221" y="0"/>
                <rect width="17" height="17" x="221" y="17" fill="#ffffbf"><title>2007-04-02: 0.2%</title></rect>
                <rect width="17" height="17" x="221" y="34" fill="#d9ef8b"><title>2007-04-03: 0.6%</title></rect>
                <rect width="17" height="17" x="221" y="51" fill="#ffffbf"><title>2007-04-04: 0.1%</title></rect>
                <rect width="17" height="17" x="221" y="68" fill="#ffffbf"><title>2007-04-05: 0.4%</title></rect>
                <rect width="17" height="17" x="221" y="85"/>
                <rect width="17" height="17" x="221" y="102"/>
                <rect width="17" height="17" x="238" y="0"/>
                <rect width="17" height="17" x="238" y="17" fill="#ffffbf"><title>2007-04-09: 0.1%</title></rect>
                <rect width="17" height="17" x="238" y="34" fill="#ffffbf"><title>2007-04-10: 0.0%</title></rect>
                <rect width="17" height="17" x="238" y="51" fill="#fee08b"><title>2007-04-11: -0.7%</title></rect>
                <rect width="17" height="17" x="238" y="68" fill="#d9ef8b"><title>2007-04-12: 0.6%</title></rect>
                <rect width="17" height="17" x="238" y="85" fill="#d9ef8b"><title>2007-04-13: 0.5%</title></rect>
                <rect width="17" height="17" x="238" y="102"/>
                <rect width="17" height="17" x="255" y="0"/>
                <rect width="17" height="17" x="255" y="17" fill="#d9ef8b"><title>2007-04-16: 0.9%</title></rect>
                <rect width="17" height="17" x="255" y="34" fill="#ffffbf"><title>2007-04-17: 0.4%</title></rect>
                <rect width="17" height="17" x="255" y="51" fill="#ffffbf"><title>2007-04-18: 0.3%</title></rect>
                <rect width="17" height="17" x="255" y="68" fill="#ffffbf"><title>2007-04-19: 0.1%</title></rect>
                <rect width="17" height="17" x="255" y="85" fill="#d9ef8b"><title>2007-04-20: 1.2%</title></rect>
                <rect width="17" height="17" x="255" y="102"/>
                <rect width="17" height="17" x="272" y="0"/>
                <rect width="17" height="17" x="272" y="17" fill="#ffffbf"><title>2007-04-23: -0.3%</title></rect>
                <rect width="17" height="17" x="272" y="34" fill="#ffffbf"><title>2007-04-24: 0.3%</title></rect>
                <rect width="17" height="17" x="272" y="51" fill="#d9ef8b"><title>2007-04-25: 1.1%</title></rect>
                <rect width="17" height="17" x="272" y="68" fill="#ffffbf"><title>2007-04-26: 0.1%</title></rect>
                <rect width="17" height="17" x="272" y="85" fill="#ffffbf"><title>2007-04-27: 0.1%</title></rect>
                <rect width="17" height="17" x="272" y="102"/>
                <rect width="17" height="17" x="289" y="0"/>
                <rect width="17" height="17" x="289" y="17" fill="#ffffbf"><title>2007-04-30: -0.4%</title></rect>
                <rect width="17" height="17" x="289" y="34" fill="#d9ef8b"><title>2007-05-01: 0.6%</title></rect>
                <rect width="17" height="17" x="289" y="51" fill="#d9ef8b"><title>2007-05-02: 0.6%</title></rect>
                <rect width="17" height="17" x="289" y="68" fill="#ffffbf"><title>2007-05-03: 0.3%</title></rect>
                <rect width="17" height="17" x="289" y="85" fill="#ffffbf"><title>2007-05-04: 0.2%</title></rect>
                <rect width="17" height="17" x="289" y="102"/>
                <rect width="17" height="17" x="306" y="0"/>
                <rect width="17" height="17" x="306" y="17" fill="#ffffbf"><title>2007-05-07: 0.4%</title></rect>
                <rect width="17" height="17" x="306" y="34" fill="#ffffbf"><title>2007-05-08: 0.0%</title></rect>
                <rect width="17" height="17" x="306" y="51" fill="#d9ef8b"><title>2007-05-09: 0.5%</title></rect>
                <rect width="17" height="17" x="306" y="68" fill="#fee08b"><title>2007-05-10: -1.1%</title></rect>
                <rect width="17" height="17" x="306" y="85" fill="#d9ef8b"><title>2007-05-11: 0.9%</title></rect>
                <rect width="17" height="17" x="306" y="102"/>
                <rect width="17" height="17" x="323" y="0"/>
                <rect width="17" height="17" x="323" y="17" fill="#ffffbf"><title>2007-05-14: 0.2%</title></rect>
                <rect width="17" height="17" x="323" y="34" fill="#ffffbf"><title>2007-05-15: 0.3%</title></rect>
                <rect width="17" height="17" x="323" y="51" fill="#d9ef8b"><title>2007-05-16: 0.8%</title></rect>
                <rect width="17" height="17" x="323" y="68" fill="#ffffbf"><title>2007-05-17: -0.1%</title></rect>
                <rect width="17" height="17" x="323" y="85" fill="#d9ef8b"><title>2007-05-18: 0.6%</title></rect>
                <rect width="17" height="17" x="323" y="102"/>
                <rect width="17" height="17" x="340" y="0"/>
                <rect width="17" height="17" x="340" y="17" fill="#ffffbf"><title>2007-05-21: -0.1%</title></rect>
                <rect width="17" height="17" x="340" y="34" fill="#ffffbf"><title>2007-05-22: 0.0%</title></rect>
                <rect width="17" height="17" x="340" y="51" fill="#ffffbf"><title>2007-05-23: -0.1%</title></rect>
                <rect width="17" height="17" x="340" y="68" fill="#fee08b"><title>2007-05-24: -0.6%</title></rect>
                <rect width="17" height="17" x="340" y="85" fill="#d9ef8b"><title>2007-05-25: 0.5%</title></rect>
                <rect width="17" height="17" x="340" y="102"> </rect>
                <rect width="17" height="17" x="357" y="0"> </rect>
                <rect width="17" height="17" x="357" y="17"> </rect>
                <rect width="17" height="17" x="357" y="34" fill="#ffffbf"><title>2007-05-29: 0.1%</title></rect>
                <rect width="17" height="17" x="357" y="51" fill="#d9ef8b"><title>2007-05-30: 0.9%</title></rect>
                <rect width="17" height="17" x="357" y="68" fill="#ffffbf"><title>2007-05-31: 0.0%</title></rect>
                <rect width="17" height="17" x="357" y="85" fill="#ffffbf"><title>2007-06-01: 0.3%</title></rect>
                <rect width="17" height="17" x="357" y="102"/>
                <rect width="17" height="17" x="374" y="0"/>
                <rect width="17" height="17" x="374" y="17" fill="#ffffbf"><title>2007-06-04: 0.1%</title></rect>
                <rect width="17" height="17" x="374" y="34" fill="#fee08b"><title>2007-06-05: -0.6%</title></rect>
                <rect width="17" height="17" x="374" y="51" fill="#fee08b"><title>2007-06-06: -0.9%</title></rect>
                <rect width="17" height="17" x="374" y="68" fill="#fdae61"><title>2007-06-07: -1.5%</title></rect>
                <rect width="17" height="17" x="374" y="85" fill="#d9ef8b"><title>2007-06-08: 1.2%</title></rect>
                <rect width="17" height="17" x="374" y="102"> </rect>
                <rect width="17" height="17" x="391" y="0"> </rect>
                <rect width="17" height="17" x="391" y="17" fill="#ffffbf"><title>2007-06-11: 0.0%</title></rect>
                <rect width="17" height="17" x="391" y="34" fill="#fee08b"><title>2007-06-12: -1.0%</title></rect>
                <rect width="17" height="17" x="391" y="51" fill="#a6d96a"><title>2007-06-13: 1.5%</title></rect>
                <rect width="17" height="17" x="391" y="68" fill="#d9ef8b"><title>2007-06-14: 0.5%</title></rect>
                <rect width="17" height="17" x="391" y="85" fill="#d9ef8b"><title>2007-06-15: 0.6%</title></rect>
                <rect width="17" height="17" x="391" y="102"> </rect>
                <rect width="17" height="17" x="408" y="0"> </rect>
                <rect width="17" height="17" x="408" y="17" fill="#ffffbf"><title>2007-06-18: -0.2%</title></rect>
                <rect width="17" height="17" x="408" y="34" fill="#ffffbf"><title>2007-06-19: 0.2%</title></rect>
                <rect width="17" height="17" x="408" y="51" fill="#fee08b"><title>2007-06-20: -1.1%</title></rect>
                <rect width="17" height="17" x="408" y="68" fill="#ffffbf"><title>2007-06-21: 0.4%</title></rect>
                <rect width="17" height="17" x="408" y="85" fill="#fdae61"><title>2007-06-22: -1.4%</title></rect>
                <rect width="17" height="17" x="408" y="102"> </rect>
                <rect width="17" height="17" x="425" y="0"> </rect>
                <rect width="17" height="17" x="425" y="17" fill="#ffffbf"><title>2007-06-25: -0.1%</title></rect>
                <rect width="17" height="17" x="425" y="34" fill="#ffffbf"><title>2007-06-26: -0.1%</title></rect>
                <rect width="17" height="17" x="425" y="51" fill="#d9ef8b"><title>2007-06-27: 0.7%</title></rect>
                <rect width="17" height="17" x="425" y="68" fill="#ffffbf"><title>2007-06-28: 0.0%</title></rect>
                <rect width="17" height="17" x="425" y="85" fill="#ffffbf"><title>2007-06-29: -0.1%</title></rect>
                <rect width="17" height="17" x="425" y="102"> </rect>
                <rect width="17" height="17" x="442" y="0"> </rect>
                <rect width="17" height="17" x="442" y="17" fill="#d9ef8b"><title>2007-07-02: 0.9%</title></rect>
                <rect width="17" height="17" x="442" y="34" fill="#ffffbf"><title>2007-07-03: 0.2%</title></rect>
                <rect width="17" height="17" x="442" y="51"> </rect>
                <rect width="17" height="17" x="442" y="68" fill="#ffffbf"><title>2007-07-05: -0.1%</title></rect>
                <rect width="17" height="17" x="442" y="85" fill="#ffffbf"><title>2007-07-06: 0.4%</title></rect>
                <rect width="17" height="17" x="442" y="102"> </rect>
                <rect width="17" height="17" x="459" y="0"> </rect>
                <rect width="17" height="17" x="459" y="17" fill="#ffffbf"><title>2007-07-09: 0.3%</title></rect>
                <rect width="17" height="17" x="459" y="34" fill="#fee08b"><title>2007-07-10: -1.1%</title></rect>
                <rect width="17" height="17" x="459" y="51" fill="#d9ef8b"><title>2007-07-11: 0.6%</title></rect>
                <rect width="17" height="17" x="459" y="68" fill="#a6d96a"><title>2007-07-12: 2.1%</title></rect>
                <rect width="17" height="17" x="459" y="85" fill="#ffffbf"><title>2007-07-13: 0.3%</title></rect>
                <rect width="17" height="17" x="459" y="102"> </rect>
                <rect width="17" height="17" x="476" y="0"> </rect>
                <rect width="17" height="17" x="476" y="17" fill="#ffffbf"><title>2007-07-16: 0.3%</title></rect>
                <rect width="17" height="17" x="476" y="34" fill="#ffffbf"><title>2007-07-17: 0.1%</title></rect>
                <rect width="17" height="17" x="476" y="51" fill="#ffffbf"><title>2007-07-18: -0.3%</title></rect>
                <rect width="17" height="17" x="476" y="68" fill="#d9ef8b"><title>2007-07-19: 0.6%</title></rect>
                <rect width="17" height="17" x="476" y="85" fill="#fee08b"><title>2007-07-20: -1.1%</title></rect>
                <rect width="17" height="17" x="476" y="102"> </rect>
                <rect width="17" height="17" x="493" y="0"> </rect>
                <rect width="17" height="17" x="493" y="17" fill="#d9ef8b"><title>2007-07-23: 0.7%</title></rect>
                <rect width="17" height="17" x="493" y="34" fill="#fdae61"><title>2007-07-24: -1.6%</title></rect>
                <rect width="17" height="17" x="493" y="51" fill="#d9ef8b"><title>2007-07-25: 0.5%</title></rect>
                <rect width="17" height="17" x="493" y="68" fill="#fdae61"><title>2007-07-26: -2.2%</title></rect>
                <rect width="17" height="17" x="493" y="85" fill="#fdae61"><title>2007-07-27: -1.5%</title></rect>
                <rect width="17" height="17" x="493" y="102"> </rect>
                <rect width="17" height="17" x="510" y="0"> </rect>
                <rect width="17" height="17" x="510" y="17" fill="#d9ef8b"><title>2007-07-30: 0.7%</title></rect>
                <rect width="17" height="17" x="510" y="34" fill="#fee08b"><title>2007-07-31: -1.1%</title></rect>
                <rect width="17" height="17" x="510" y="51" fill="#d9ef8b"><title>2007-08-01: 1.1%</title></rect>
                <rect width="17" height="17" x="510" y="68" fill="#d9ef8b"><title>2007-08-02: 0.8%</title></rect>
                <rect width="17" height="17" x="510" y="85" fill="#fdae61"><title>2007-08-03: -2.1%</title></rect>
                <rect width="17" height="17" x="510" y="102"> </rect>
                <rect width="17" height="17" x="527" y="0"> </rect>
                <rect width="17" height="17" x="527" y="17" fill="#a6d96a"><title>2007-08-06: 2.2%</title></rect>
                <rect width="17" height="17" x="527" y="34" fill="#ffffbf"><title>2007-08-07: 0.3%</title></rect>
                <rect width="17" height="17" x="527" y="51" fill="#d9ef8b"><title>2007-08-08: 1.2%</title></rect>
                <rect width="17" height="17" x="527" y="68" fill="#f46d43"><title>2007-08-09: -2.8%</title></rect>
                <rect width="17" height="17" x="527" y="85" fill="#ffffbf"><title>2007-08-10: -0.2%</title></rect>
                <rect width="17" height="17" x="527" y="102"> </rect>
                <rect width="17" height="17" x="544" y="0"> </rect>
                <rect width="17" height="17" x="544" y="17" fill="#ffffbf"><title>2007-08-13: 0.0%</title></rect>
                <rect width="17" height="17" x="544" y="34" fill="#fdae61"><title>2007-08-14: -1.6%</title></rect>
                <rect width="17" height="17" x="544" y="51" fill="#fee08b"><title>2007-08-15: -1.2%</title></rect>
                <rect width="17" height="17" x="544" y="68" fill="#ffffbf"><title>2007-08-16: -0.1%</title></rect>
                <rect width="17" height="17" x="544" y="85" fill="#a6d96a"><title>2007-08-17: 1.8%</title></rect>
                <rect width="17" height="17" x="544" y="102"> </rect>
                <rect width="17" height="17" x="561" y="0"> </rect>
                <rect width="17" height="17" x="561" y="17" fill="#ffffbf"><title>2007-08-20: 0.3%</title></rect>
                <rect width="17" height="17" x="561" y="34" fill="#ffffbf"><title>2007-08-21: -0.2%</title></rect>
                <rect width="17" height="17" x="561" y="51" fill="#d9ef8b"><title>2007-08-22: 1.1%</title></rect>
                <rect width="17" height="17" x="561" y="68" fill="#ffffbf"><title>2007-08-23: 0.0%</title></rect>
                <rect width="17" height="17" x="561" y="85" fill="#d9ef8b"><title>2007-08-24: 1.1%</title></rect>
                <rect width="17" height="17" x="561" y="102"> </rect>
                <rect width="17" height="17" x="578" y="0"> </rect>
                <rect width="17" height="17" x="578" y="17" fill="#ffffbf"><title>2007-08-27: -0.4%</title></rect>
                <rect width="17" height="17" x="578" y="34" fill="#fdae61"><title>2007-08-28: -2.1%</title></rect>
                <rect width="17" height="17" x="578" y="51" fill="#a6d96a"><title>2007-08-29: 1.9%</title></rect>
                <rect width="17" height="17" x="578" y="68" fill="#ffffbf"><title>2007-08-30: -0.4%</title></rect>
                <rect width="17" height="17" x="578" y="85" fill="#d9ef8b"><title>2007-08-31: 0.9%</title></rect>
                <rect width="17" height="17" x="578" y="102"> </rect>
                <rect width="17" height="17" x="595" y="0"> </rect>
                <rect width="17" height="17" x="595" y="17"> </rect>
                <rect width="17" height="17" x="595" y="34" fill="#d9ef8b"><title>2007-09-04: 0.7%</title></rect>
                <rect width="17" height="17" x="595" y="51" fill="#fee08b"><title>2007-09-05: -1.0%</title></rect>
                <rect width="17" height="17" x="595" y="68" fill="#ffffbf"><title>2007-09-06: 0.4%</title></rect>
                <rect width="17" height="17" x="595" y="85" fill="#fdae61"><title>2007-09-07: -1.9%</title></rect>
                <rect width="17" height="17" x="595" y="102"> </rect>
                <rect width="17" height="17" x="612" y="0"> </rect>
                <rect width="17" height="17" x="612" y="17" fill="#ffffbf"><title>2007-09-10: 0.1%</title></rect>
                <rect width="17" height="17" x="612" y="34" fill="#d9ef8b"><title>2007-09-11: 1.4%</title></rect>
                <rect width="17" height="17" x="612" y="51" fill="#ffffbf"><title>2007-09-12: -0.1%</title></rect>
                <rect width="17" height="17" x="612" y="68" fill="#d9ef8b"><title>2007-09-13: 1.0%</title></rect>
                <rect width="17" height="17" x="612" y="85" fill="#ffffbf"><title>2007-09-14: 0.2%</title></rect>
                <rect width="17" height="17" x="612" y="102"> </rect>
                <rect width="17" height="17" x="629" y="0"> </rect>
                <rect width="17" height="17" x="629" y="17" fill="#ffffbf"><title>2007-09-17: -0.3%</title></rect>
                <rect width="17" height="17" x="629" y="34" fill="#66bd63"><title>2007-09-18: 2.5%</title></rect>
                <rect width="17" height="17" x="629" y="51" fill="#d9ef8b"><title>2007-09-19: 0.5%</title></rect>
                <rect width="17" height="17" x="629" y="68" fill="#ffffbf"><title>2007-09-20: -0.3%</title></rect>
                <rect width="17" height="17" x="629" y="85" fill="#ffffbf"><title>2007-09-21: 0.4%</title></rect>
                <rect width="17" height="17" x="629" y="102"> </rect>
                <rect width="17" height="17" x="646" y="0"> </rect>
                <rect width="17" height="17" x="646" y="17" fill="#ffffbf"><title>2007-09-24: -0.5%</title></rect>
                <rect width="17" height="17" x="646" y="34" fill="#ffffbf"><title>2007-09-25: 0.2%</title></rect>
                <rect width="17" height="17" x="646" y="51" fill="#d9ef8b"><title>2007-09-26: 0.7%</title></rect>
                <rect width="17" height="17" x="646" y="68" fill="#ffffbf"><title>2007-09-27: 0.2%</title></rect>
                <rect width="17" height="17" x="646" y="85" fill="#ffffbf"><title>2007-09-28: -0.1%</title></rect>
                <rect width="17" height="17" x="646" y="102"> </rect>
                <rect width="17" height="17" x="663" y="0"> </rect>
                <rect width="17" height="17" x="663" y="17" fill="#a6d96a"><title>2007-10-01: 1.4%</title></rect>
                <rect width="17" height="17" x="663" y="34" fill="#ffffbf"><title>2007-10-02: -0.3%</title></rect>
                <rect width="17" height="17" x="663" y="51" fill="#fee08b"><title>2007-10-03: -0.5%</title></rect>
                <rect width="17" height="17" x="663" y="68" fill="#ffffbf"><title>2007-10-04: 0.0%</title></rect>
                <rect width="17" height="17" x="663" y="85" fill="#d9ef8b"><title>2007-10-05: 0.7%</title></rect>
                <rect width="17" height="17" x="663" y="102"> </rect>
                <rect width="17" height="17" x="680" y="0"> </rect>
                <rect width="17" height="17" x="680" y="17" fill="#ffffbf"><title>2007-10-08: -0.2%</title></rect>
                <rect width="17" height="17" x="680" y="34" fill="#d9ef8b"><title>2007-10-09: 0.9%</title></rect>
                <rect width="17" height="17" x="680" y="51" fill="#fee08b"><title>2007-10-10: -0.6%</title></rect>
                <rect width="17" height="17" x="680" y="68" fill="#ffffbf"><title>2007-10-11: -0.5%</title></rect>
                <rect width="17" height="17" x="680" y="85" fill="#d9ef8b"><title>2007-10-12: 0.5%</title></rect>
                <rect width="17" height="17" x="680" y="102"> </rect>
                <rect width="17" height="17" x="697" y="0"> </rect>
                <rect width="17" height="17" x="697" y="17" fill="#fee08b"><title>2007-10-15: -0.8%</title></rect>
                <rect width="17" height="17" x="697" y="34" fill="#fee08b"><title>2007-10-16: -0.5%</title></rect>
                <rect width="17" height="17" x="697" y="51" fill="#ffffbf"><title>2007-10-17: -0.2%</title></rect>
                <rect width="17" height="17" x="697" y="68" fill="#ffffbf"><title>2007-10-18: 0.0%</title></rect>
                <rect width="17" height="17" x="697" y="85" fill="#f46d43"><title>2007-10-19: -2.6%</title></rect>
                <rect width="17" height="17" x="697" y="102"> </rect>
                <rect width="17" height="17" x="714" y="0"> </rect>
                <rect width="17" height="17" x="714" y="17" fill="#ffffbf"><title>2007-10-22: 0.3%</title></rect>
                <rect width="17" height="17" x="714" y="34" fill="#d9ef8b"><title>2007-10-23: 0.8%</title></rect>
                <rect width="17" height="17" x="714" y="51" fill="#ffffbf"><title>2007-10-24: 0.0%</title></rect>
                <rect width="17" height="17" x="714" y="68" fill="#ffffbf"><title>2007-10-25: 0.0%</title></rect>
                <rect width="17" height="17" x="714" y="85" fill="#d9ef8b"><title>2007-10-26: 1.0%</title></rect>
                <rect width="17" height="17" x="714" y="102"> </rect>
                <rect width="17" height="17" x="731" y="0"> </rect>
                <rect width="17" height="17" x="731" y="17" fill="#d9ef8b"><title>2007-10-29: 0.5%</title></rect>
                <rect width="17" height="17" x="731" y="34" fill="#fee08b"><title>2007-10-30: -0.6%</title></rect>
                <rect width="17" height="17" x="731" y="51" fill="#d9ef8b"><title>2007-10-31: 1.0%</title></rect>
                <rect width="17" height="17" x="731" y="68" fill="#f46d43"><title>2007-11-01: -2.6%</title></rect>
                <rect width="17" height="17" x="731" y="85" fill="#ffffbf"><title>2007-11-02: 0.2%</title></rect>
                <rect width="17" height="17" x="731" y="102"> </rect>
                <rect width="17" height="17" x="748" y="0"> </rect>
                <rect width="17" height="17" x="748" y="17" fill="#ffffbf"><title>2007-11-05: -0.4%</title></rect>
                <rect width="17" height="17" x="748" y="34" fill="#d9ef8b"><title>2007-11-06: 0.9%</title></rect>
                <rect width="17" height="17" x="748" y="51" fill="#f46d43"><title>2007-11-07: -2.5%</title></rect>
                <rect width="17" height="17" x="748" y="68" fill="#ffffbf"><title>2007-11-08: -0.3%</title></rect>
                <rect width="17" height="17" x="748" y="85" fill="#fdae61"><title>2007-11-09: -1.6%</title></rect>
                <rect width="17" height="17" x="748" y="102"> </rect>
                <rect width="17" height="17" x="765" y="0"> </rect>
                <rect width="17" height="17" x="765" y="17" fill="#ffffbf"><title>2007-11-12: -0.4%</title></rect>
                <rect width="17" height="17" x="765" y="34" fill="#66bd63"><title>2007-11-13: 2.6%</title></rect>
                <rect width="17" height="17" x="765" y="51" fill="#fee08b"><title>2007-11-14: -0.6%</title></rect>
                <rect width="17" height="17" x="765" y="68" fill="#fee08b"><title>2007-11-15: -0.9%</title></rect>
                <rect width="17" height="17" x="765" y="85" fill="#d9ef8b"><title>2007-11-16: 0.5%</title></rect>
                <rect width="17" height="17" x="765" y="102"> </rect>
                <rect width="17" height="17" x="782" y="0"> </rect>
                <rect width="17" height="17" x="782" y="17" fill="#fdae61"><title>2007-11-19: -1.7%</title></rect>
                <rect width="17" height="17" x="782" y="34" fill="#ffffbf"><title>2007-11-20: 0.4%</title></rect>
                <rect width="17" height="17" x="782" y="51" fill="#fdae61"><title>2007-11-21: -1.6%</title></rect>
                <rect width="17" height="17" x="782" y="68"> </rect>
                <rect width="17" height="17" x="782" y="85" fill="#d9ef8b"><title>2007-11-23: 0.7%</title></rect>
                <rect width="17" height="17" x="782" y="102"> </rect>
                <rect width="17" height="17" x="799" y="0"> </rect>
                <rect width="17" height="17" x="799" y="17" fill="#fdae61"><title>2007-11-26: -1.8%</title></rect>
                <rect width="17" height="17" x="799" y="34" fill="#a6d96a"><title>2007-11-27: 1.7%</title></rect>
                <rect width="17" height="17" x="799" y="51" fill="#66bd63"><title>2007-11-28: 2.6%</title></rect>
                <rect width="17" height="17" x="799" y="68" fill="#ffffbf"><title>2007-11-29: 0.2%</title></rect>
                <rect width="17" height="17" x="799" y="85" fill="#ffffbf"><title>2007-11-30: 0.4%</title></rect>
                <rect width="17" height="17" x="799" y="102"> </rect>
                <rect width="17" height="17" x="816" y="0"> </rect>
                <rect width="17" height="17" x="816" y="17" fill="#ffffbf"><title>2007-12-03: -0.4%</title></rect>
                <rect width="17" height="17" x="816" y="34" fill="#fee08b"><title>2007-12-04: -0.5%</title></rect>
                <rect width="17" height="17" x="816" y="51" fill="#a6d96a"><title>2007-12-05: 1.5%</title></rect>
                <rect width="17" height="17" x="816" y="68" fill="#d9ef8b"><title>2007-12-06: 1.3%</title></rect>
                <rect width="17" height="17" x="816" y="85" fill="#ffffbf"><title>2007-12-07: 0.1%</title></rect>
                <rect width="17" height="17" x="816" y="102"> </rect>
                <rect width="17" height="17" x="833" y="0"> </rect>
                <rect width="17" height="17" x="833" y="17" fill="#d9ef8b"><title>2007-12-10: 0.8%</title></rect>
                <rect width="17" height="17" x="833" y="34" fill="#fdae61"><title>2007-12-11: -2.1%</title></rect>
                <rect width="17" height="17" x="833" y="51" fill="#ffffbf"><title>2007-12-12: 0.3%</title></rect>
                <rect width="17" height="17" x="833" y="68" fill="#ffffbf"><title>2007-12-13: 0.3%</title></rect>
                <rect width="17" height="17" x="833" y="85" fill="#fee08b"><title>2007-12-14: -1.3%</title></rect>
                <rect width="17" height="17" x="833" y="102"> </rect>
                <rect width="17" height="17" x="850" y="0"> </rect>
                <rect width="17" height="17" x="850" y="17" fill="#fee08b"><title>2007-12-17: -1.3%</title></rect>
                <rect width="17" height="17" x="850" y="34" fill="#d9ef8b"><title>2007-12-18: 0.5%</title></rect>
                <rect width="17" height="17" x="850" y="51" fill="#ffffbf"><title>2007-12-19: -0.2%</title></rect>
                <rect width="17" height="17" x="850" y="68" fill="#ffffbf"><title>2007-12-20: 0.3%</title></rect>
                <rect width="17" height="17" x="850" y="85" fill="#a6d96a"><title>2007-12-21: 1.6%</title></rect>
                <rect width="17" height="17" x="850" y="102"> </rect>
                <rect width="17" height="17" x="867" y="0"> </rect>
                <rect width="17" height="17" x="867" y="17" fill="#d9ef8b"><title>2007-12-24: 0.5%</title></rect>
                <rect width="17" height="17" x="867" y="34"> </rect>
                <rect width="17" height="17" x="867" y="51" fill="#ffffbf"><title>2007-12-26: 0.0%</title></rect>
                <rect width="17" height="17" x="867" y="68" fill="#fdae61"><title>2007-12-27: -1.4%</title></rect>
                <rect width="17" height="17" x="867" y="85" fill="#ffffbf"><title>2007-12-28: 0.0%</title></rect>
                <rect width="17" height="17" x="867" y="102"/>
                <rect width="17" height="17" x="884" y="0"> </rect>
                <rect width="17" height="17" x="884" y="17" fill="#fee08b"><title>2007-12-31: -0.7%</title></rect>
            </g>
            <g fill="none" stroke="#000">
                <path d="M17,17H0V119H68V68H85V0H17Z"/>
                <path d="M85,68H68V119H136V68H153V0H85Z"> </path>
                <path d="M153,68H136V119H204V119H221V0H153Z"> </path>
                <path d="M238,0H221V119H289V34H306V0H238Z"> </path>
                <path d="M306,34H289V119H357V85H374V0H306Z"> </path>
                <path d="M374,85H357V119H425V119H442V0H374Z"> </path>
                <path d="M459,0H442V119H510V51H527V0H459Z"> </path>
                <path d="M527,51H510V119H578V102H595V0H527Z"> </path>
                <path d="M595,102H578V119H663V17H680V0H595Z"> </path>
                <path d="M680,17H663V119H731V68H748V0H680Z"> </path>
                <path d="M748,68H731V119H799V102H816V0H748Z"> </path>
                <path d="M816,102H799V119H884V34H901V0H816Z"> </path>
            </g>
        </g>
    </svg>
);
